from flask import Flask

from projectname.somecode import hello

app = Flask(__name__)


@app.route('/')
def home():
    return hello("Sweetie")

@app.route('/<name>')
def test(name):
    return hello(name)
