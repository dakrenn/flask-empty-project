Run in development mode:
```
$ FLASK_APP=projectname/views.py FLASK_ENV=development python manage.py run
```

Dockerizing inspired by:
https://testdriven.io/blog/dockerizing-flask-with-postgres-gunicorn-and-nginx/

