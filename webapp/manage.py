from flask.cli import FlaskGroup

from projectname.views import app


cli = FlaskGroup(app)


if __name__ == "__main__":
    cli()
