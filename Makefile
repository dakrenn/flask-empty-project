.PHONY: build devsys-up devsys-down deploy-up deploy-down

build:
	docker-compose build

devsys-up:
	docker-compose up

devsys-down:
	docker-compose down

deploy-up:
	docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d

deploy-down:
	docker-compose down
